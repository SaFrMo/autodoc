﻿# AutoDoc

## What

AutoDoc is an app that syncs C# files with their documentation. It’s developed specifically for use in the Unity game engine and the MonoDevelop IDE but can be used with any C# project.

AutoDoc is an open-source project developed by [Raconteur Games](http://raconteurgames.com/).

## Setup

# Documentation

## Example Workflow Tutorial

Documentation and implementation go hand-in-hand in the AutoDoc workflow, ensuring that your code and your thought processes stay synced. Let's take a look at an example project to see what this means.

1. Place the AutoDoc .exe file into the root folder that will contain all of your scripts. Double-click AutoDoc to start the app.
2. AutoDoc will create an `_autodoc.md` folder in each directory (including the root directory) that will reflect the current state of all classes in that directory, including their public methods and properties. You can also edit the `_autodoc.md` file itself to create and update classes, as well as their public properties and methods.
3. Open the `_autodoc.md` file that was just created in the root directory. You should see a file with the auto-generated boilerplate header and footer. See `boilerplate.md` for more information.
4. Add in your first class! Follow the instructions in the boilerplate to create and document your first class. Note that __saving this `_autodoc.md` file will execute code creation and updating__, so frequent backups are recommended to avoid losing work.

# Under the Hood

## What's Happening?

When the user starts the AutoDoc app, the app starts listening for a change to any `.md` and `.cs` file contained in the app's directory.

### On `.md` file change
When `.md` file `x` changes, AutoDoc:

1. Regex removes the contents of `boilerplate.md` from `x`
2. Regex matches each `h1`-divided section `section`
3. Runs `CreateOrUpdate(section)`

### On `.cs` file change
When `.cs` file `y` changes, AutoDoc:

1. Runs `var entry = FindOrCreateEntry(y.filename)`
2. Scans `y` for all public methods and properties `publicMembers`
3. Runs `UpdateEntry(entry, publicMembers)`

# Todo

* Finish C#-to-MD parsing
* MD-to-C# parsing
* Listen for changes and sync files correctly