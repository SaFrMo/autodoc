"use strict";

// TODO
// 1. Check if .cs file for class in Readme exists and edit rather than rewrite





// App modules
const	fs			= require('fs'),
		glob		= require('glob'),
		chokidar	= require('chokidar'),
		os			= require('os');

// Make sure that each directory has a readme.md	
const fillReadmes = function() {
	glob('!(node_modules)/**/', null, (er, allDirs) => {
		// Add cwd
		allDirs.push('./');
		// Get boilerplate contents
		let boilerplate = fs.readFileSync('./boilerplate.md', 'utf-8');
		let boilerplateParts = boilerplate.split('{{DOCUMENTATION}}');
		// allDirs is an array of all directories
		for (let i = 0; i < allDirs.length; i++) {
			// Place boilerplate in all readmes
			let d = allDirs[i];
			let readmePath = d + 'readme.md';
			
			// Find the readme in this directory or create it if it doesn't exist
			readOrCreate(readmePath);
			
			// Turn C# files into readme contents
			let contents = parseMdIntoCs(readmePath);
			
			
			
			
			//let contents = boilerplateParts[0].replace('{{DIRECTORY}}', d);
			
			
			// Parse MD to C#
			
			
			/*
			// Document existing classes in dir
			let filesInDir = fs.readdirSync(d);
			// Find all C# files
			let csFiles = filesInDir.filter(item => {
				return /.\.cs$/.test(item);
			});
			// Set up docs for C# files
			csFiles.forEach(file => {
				contents += parseCsIntoReadme(d + file);
			});
			// Add boilerplate footer
			contents += boilerplateParts[1];
			
			// Write docs
			fs.writeFileSync(readmePath, contents);
			*/
			
			//console.log('Readme complete for ' + d);
		}
		
		
	});
}

const 	parseCsIntoReadme = function(filePath) {
	let toReturn = '';
	
	// Save contents of file path
	let contents = fs.readFileSync(filePath, 'utf-8');
	// Find public vars
	//let publicVars = contents.match(/public\s+(.*?)\s+(.*?)[;{]/g);
	let publicVars = contents.match(/(?:(?:\s?\/\/\/ <summary>)[\r\n]?\s?([\W\w]*)[\r\n]?(?:\s?\/\/\/ <\/summary>))?[\r\n]?(?:(?:public\s+)(.*?)\s+(.*?)[;{])/g);
	let publicVarDocs = '';
	if ( publicVars != null ) {
		publicVars.forEach(v => {
			// Remove leading 'public'
			v = v.replace(/^public /g, '');
			// Remove ending { or ;
			v = v.replace(/[{;]$/g, '');
			// Change tabs and multiple whitespaces to single whitespace
			v = v.replace(/\t+/g, ' ').replace(/\s{2,}/g, ' ');
			console.log(v);
			// Prep doc entry
			let newEntry = createMemberDoc(v);
			publicVarDocs += newEntry;
		});
	}
	
	toReturn += publicVarDocs;
	
	return toReturn;
}

const	createMemberDoc = function(sourceMember) {
	let toReturn = '/// <summary>\n';
	
	// 
	
	toReturn += '/// </summary>\n';
	
	toReturn += '\n\n';
	return toReturn;
}

const	parseMdIntoCs = function(filePath) {
	let toReturn = '';
	let dir = filePath.replace(/[^\/]*$/, '');
	// Save contents of MD
	let contents = fs.readFileSync(filePath, 'utf-8');
	// Strip boilerplate
	let stripped = contents.replace(/^.*[\r\n]*#\s/, '').replace(/# _Reference_[\w\W]*/, '');
	// Separate into h1s
	let classes = stripped.split(/[^#]#\s/);
	classes.forEach(x => {
		let result = '';
		let rawClassName = x.match(/^([\w\W]*?)[\r\n]/)[1];
		let className = rawClassName;
		if ( ! className.includes(':') ) {
			// Inherit from MonoBehaviour if no inheritance specified
			className += ' : MonoBehaviour';
		}
		
		// Find the class's name from this entry
		rawClassName = rawClassName.replace(/\s*:[\w\W]*/, '');
		
		// Pull the class documentation from this entry
		let classDoc = x.match(/^[^\r\n]*[\r\n]*([^\r\n]*)/)[1];
		
		// Check if .cs file already exists, edit if so
		let filename = rawClassName + '.cs';
		readOrCreate(filename, () => {
			// .cs file already exists
			let existingData = fs.readFileSync(filename, 'utf-8');
			console.log(existingData);
		}, () => {
			// .cs file just created
			
			// Put the class documentation into the proper place
			result = '/// <summary>\n/// ' + classDoc + '\n/// </summary>\npublic class ' + className + ' {\n\n\n}\n\n';
			result.replace('\n', os.EOL);
			
			fs.writeFileSync(dir + rawClassName + '.cs', result);
			
			console.log('Created ' + filename + ' for class ' + rawClassName);
		});
		
		
		
		
		//toReturn += result;
		
	});
	
	return toReturn;
}

const	readOrCreate = function(path, onExist, onCreated) {
	try {
		// File exists
		fs.accessSync(path);
		// Fire callback
		if (onExist) onExist();
	} catch (e) {
		// File doesn't exist in this directory, so create it
		fs.openSync(path, 'a+');
		// Fire callback
		if (onCreated) onCreated();
	}
}






const	app = function(){
	fillReadmes();
}

app();

return;	


// .md watcher
let mdWatcher = chokidar.watch('**/*.md', {
	ignored: /[\/\\]\./,
	persistent: true
});

mdWatcher
	.on('add', path => onMdUpdate(path) )
	.on('change', path => onMdChange(path) );
	
const	onMdUpdate = function(path){
	console.log(path);
}

const 	onMdChange = function(path){
	console.log(path + ' changed');
}

// .cs watcher
let csWatcher = chokidar.watch('**/*.cs', {
	ignored: /[\/\\]\./,
	persistent: true
});

csWatcher
	.on('add', path => onCsUpdate(path) )
	.on('change', path => oncsChange(path) );
	
const	onCsUpdate = function(path){
	console.log(path);
}

const 	onCsChange = function(path){
	console.log(path + ' changed');
}
